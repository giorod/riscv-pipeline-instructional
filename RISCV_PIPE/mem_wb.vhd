library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity mem_wb is
	port(
        clk: in std_logic;
        
        MEMWB_ctrl_in:      in std_logic_vector(1 downto 0); --MemToReg,RegWrite
        MEMWB_ALUresult_in: in std_logic_vector(63 downto 0);
        MEMWB_readdata_in:  in std_logic_vector(63 downto 0);
        MEMWB_rd_in:        in std_logic_vector(4 downto 0);

        MEMWB_ctrl_out:      out std_logic_vector(1 downto 0) := (others => '0'); --MemToReg,RegWrite
        MEMWB_ALUresult_out: out std_logic_vector(63 downto 0) := (others => '0');
        MEMWB_readdata_out:  out std_logic_vector(63 downto 0) := (others => '0');
        MEMWB_rd_out:        out std_logic_vector(4 downto 0) := (others => '0')
    );
end entity;

architecture BEHAVIORAL of mem_wb is 	
begin

    MEMWB_update: process(clk)
    begin    
        if rising_edge(clk) then
            MEMWB_ctrl_out <= MEMWB_ctrl_in; --MemToReg,RegWrite
            MEMWB_ALUresult_out <= MEMWB_ALUresult_in;
            MEMWB_readdata_out <= MEMWB_readdata_in;
            MEMWB_rd_out <= MEMWB_rd_in;
        end if;
    end process MEMWB_update;

end BEHAVIORAL;