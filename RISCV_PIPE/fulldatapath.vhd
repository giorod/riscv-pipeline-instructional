library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity fulldatapath is
    port (
        clk: in std_logic;
        rst: in std_logic
    );
end entity fulldatapath;

architecture structural of fulldatapath is
    component if_block is
        port (
            clk: in std_logic;
            rst: in std_logic;
            PCwrite: in std_logic;
            PCsrc: in std_logic;
            BranchAddress: in std_logic_vector(63 downto 0);
            IF_instr: out std_logic_vector(31 downto 0);
            IF_PC: out std_logic_vector(63 downto 0)
        );
    end component if_block;

    component if_id is
        port (
            clk: in std_logic;
            IF_flush: in std_logic;
            IFID_write: in std_logic;
            instr_in: in std_logic_vector(31 downto 0) := (others => '0');
            PC_in: in std_logic_vector(63 downto 0) := (others => '0');
            instr_out: out std_logic_vector(31 downto 0);
            PC_out: out std_logic_vector(63 downto 0)
        );
    end component if_id;

    component id_block is
        port (
            clk: in std_logic;
            ID_PC_in: in std_logic_vector(63 downto 0);
            ID_instr_in: in std_logic_vector(31 downto 0);
            regWrite: in std_logic; --comes from wb stage
            WriteData: in std_logic_vector(63 downto 0); -- comes from wb stage
            WriteAddr: in std_logic_vector(4 downto 0); -- comes from wb stage
    
            data1: out std_logic_vector(63 downto 0);
            data2: out std_logic_vector(63 downto 0);
            
            ID_instr_out: out std_logic_vector(31 downto 0); -- needed for the hazard detection and subsequent stages
            ID_ctl: out std_logic_vector(7 downto 0); -- control bits
            imm: out std_logic_vector(63 downto 0); -- calculated immediate, used in ex stage
            ID_PC_out: out std_logic_vector(63 downto 0)
            --ADD HAZARD LOGIC
    
        );
    end component id_block;

    component id_ex is
        port (
            clk: in std_logic;
            IDEX_flush: in std_logic;
    
            IDEX_ctl_in: in std_logic_vector(7 downto 0);
            IDEX_PC_in: in std_logic_vector(63 downto 0);
            IDEX_instr_in: in std_logic_vector(31 downto 0);
            IDEX_imm_in: in std_logic_vector(63 downto 0);
            IDEX_dataA_in: in std_logic_vector(63 downto 0);
            IDEX_dataB_in: in std_logic_vector(63 downto 0);
    
            IDEX_ctl_out:   out std_logic_vector(7 downto 0);
            IDEX_PC_out:    out std_logic_vector(63 downto 0);
            IDEX_instr_out: out std_logic_vector(31 downto 0);
            IDEX_imm_out:   out std_logic_vector(63 downto 0);
            IDEX_dataA_out: out std_logic_vector(63 downto 0);
            IDEX_dataB_out: out std_logic_vector(63 downto 0)
    
        );
    end component;

    component ex_block is -- The stage registers are clock syncronized
        port (
    
            EX_PC_in: in std_logic_vector(63 downto 0);
            EX_instr_in: in std_logic_vector(31 downto 0); 
            EX_imm_in: in std_logic_vector(63 downto 0);
    
            ID_dataA: in std_logic_vector(63 downto 0);
            WB_dataA: in std_logic_vector(63 downto 0);
            MEM_dataA: in std_logic_vector(63 downto 0);
    
            ID_dataB: in std_logic_vector(63 downto 0);
            WB_dataB: in std_logic_vector(63 downto 0);
            MEM_dataB: in std_logic_vector(63 downto 0);
    
            MEM_rd: in std_logic_vector(4 downto 0); -- Forwarding inputs
            WB_rd: in std_logic_vector(4 downto 0);
            MEM_RegWrite: in std_logic;
            WB_RegWrite: in std_logic;
    
            ALUsrc: in std_logic;
            ALUOp: in std_logic_vector(1 downto 0);
    
            ALUResult: out std_logic_vector(63 downto 0);
            BranchPC: out std_logic_vector(63 downto 0);
            WriteData: out std_logic_vector(63 downto 0);
            BranchResult: out std_logic
    
            -- ADD HAZARD OUTPUTS
    
        );
    end component ex_block;

    component ex_mem is
        port(
            clk: in std_logic;
            EXMEM_flush: in std_logic;
            
            EXMEM_ctrl_in:      in std_logic_vector(4 downto 0);
            EXMEM_ALUresult_in: in std_logic_vector(63 downto 0);
            EXMEM_dataB_in:     in std_logic_vector(63 downto 0);
            EXMEM_rd_in:        in std_logic_vector(4 downto 0);
            EXMEM_branchPC_in:  in std_logic_vector(63 downto 0);
            EXMEM_brtaken_in:   in std_logic;
    
            EXMEM_ctrl_out:      out std_logic_vector(4 downto 0);
            EXMEM_ALUresult_out: out std_logic_vector(63 downto 0);
            EXMEM_dataB_out:     out std_logic_vector(63 downto 0);
            EXMEM_rd_out:        out std_logic_vector(4 downto 0);
            EXMEM_branchPC_out:  out std_logic_vector(63 downto 0);
            EXMEM_brtaken_out:   out std_logic
    
        );
    end component;

    component mem_block is
        port (
            MEM_clk: in std_logic;
            MEM_branch_ctl: in std_logic;
            MEM_branch_true: in std_logic;
            MEM_memwrite: in std_logic;
            MEM_memread: in std_logic;
    
            MEM_address: in std_logic_vector(63 downto 0);
            MEM_datawrite: in std_logic_vector(63 downto 0);
    
            MEM_readdata: out std_logic_vector(63 downto 0);
            MEM_aluresult: out std_logic_vector(63 downto 0);
            PCSrc: out std_logic
    
        );
    end component mem_block;
    
    component mem_wb is
        port(
            clk: in std_logic;
            
            MEMWB_ctrl_in:      in std_logic_vector(1 downto 0); --MemToReg,RegWrite
            MEMWB_ALUresult_in: in std_logic_vector(63 downto 0);
            MEMWB_readdata_in:  in std_logic_vector(63 downto 0);
            MEMWB_rd_in:        in std_logic_vector(4 downto 0);
    
            MEMWB_ctrl_out:      out std_logic_vector(1 downto 0); --MemToReg,RegWrite
            MEMWB_ALUresult_out: out std_logic_vector(63 downto 0);
            MEMWB_readdata_out:  out std_logic_vector(63 downto 0);
            MEMWB_rd_out:        out std_logic_vector(4 downto 0)
        );
    end component;

    component mux2to1_64 is --used in WB stage
        port(
            a: in std_logic_vector(63 downto 0);
            b: in std_logic_vector(63 downto 0);
            s: in std_logic;
            z: out std_logic_vector(63 downto 0)
        );
    end component mux2to1_64;

    component hazard is -- Hazard detection unit logic
        port (
          IDEX_memread: in std_logic;
          IDEX_rd: in std_logic_vector(4 downto 0);
          IFID_rs1: in std_logic_vector(4 downto 0);
          IFID_rs2: in std_logic_vector(4 downto 0);
          cmux: out std_logic;
          PCwrite: out std_logic;
          IFID_write: out std_logic
        );
      end component hazard;

    -- IF STAGE SIGNALS

    signal PCwrite, PCsrc: std_logic := '0';
    signal IF_PC: std_logic_vector(63 downto 0) := (others => '0');
    signal IF_instr: std_logic_vector(31 downto 0) := (others => '0');

    --IF_ID register additional signals

    signal IFID_instr: std_logic_vector(31 downto 0) := (others => '0');
    signal IFID_PC: std_logic_vector(63 downto 0) := (others => '0');

    -- ID STAGE SIGNALS

    signal datars1, datars2, ID_imm, ID_PC: std_logic_vector(63 downto 0) := (others => '0');
    signal ID_ctl, ID_ctl_hazard: std_logic_vector(7 downto 0) := (others => '0');
    signal ID_instr: std_logic_vector(31 downto 0) := (others => '0');

    -- IDEX register signals

    signal IDEX_datars1, IDEX_datars2, IDEX_imm, IDEX_PC: std_logic_vector(63 downto 0) := (others => '0');
    signal IDEX_ctl: std_logic_vector(7 downto 0) := (others => '0');
    signal IDEX_instr: std_logic_vector(31 downto 0) := (others => '0');

    -- EX stage outputs

    signal ALUResult, BranchPC, WriteData: std_logic_vector(63 downto 0) := (others => '0');
    signal BranchResult: std_logic := '0';

    -- EXMEM register signals

    signal EXMEM_ctl, EXMEM_rd: std_logic_vector(4 downto 0) := (others => '0');
    signal EXMEM_alures, EXMEM_wdata, EXMEM_branchPC: std_logic_vector(63 downto 0) := (others => '0');
    signal EXMEM_branchres: std_logic := '0';

    -- MEM stage outputs

    signal MEM_readdata, MEM_aluresult: std_logic_vector(63 downto 0) := (others => '0');
    -- signal PCSrc: std_logic; already defined

    -- MEMWB register signals, used in WB stage built here

    signal WB_ctl: std_logic_vector(1 downto 0) := (others => '0');
    signal WB_alu, WB_data, WB_regreturn: std_logic_vector(63 downto 0) := (others => '0');
    signal WB_rd: std_logic_vector(4 downto 0) := (others => '0');

    --- Hazard detection unit outputs, PCwrite defined before

    signal cmux, IFID_write: std_logic := '0';



begin

    -- Registers are flushed if branch is taken, so if PCSrc is 1
    
    if_stage_map: if_block Port Map(
        clk => clk,
        rst => rst,
        PCwrite => PCwrite,
        PCsrc => PCsrc,
        BranchAddress => EXMEM_branchPC, -- Attento l'hai ridefinito dopo!, lo prendi dal registro exmem
        IF_instr => IF_instr,
        IF_PC => IF_PC
    );

    if_id_reg: if_id Port Map(
        clk => clk,
        IF_flush => PCSrc, -- Flush on branch taken
        IFID_write => IFID_write, -- Controls the stall on load hazard
        instr_in => IF_instr,
        PC_in => IF_PC,
        instr_out => IFID_instr,
        PC_out => IFID_PC
    );

    id_stage_map: id_block Port Map(
        clk => clk,
        ID_PC_in => IFID_PC,
        ID_instr_in => IFID_instr,
        regWrite => WB_ctl(1), -- taken from WB stage
        WriteData => WB_regreturn,
        WriteAddr => WB_rd,
        data1 => datars1,
        data2 => datars2,
        ID_instr_out => ID_instr,
        ID_ctl => ID_ctl,
        imm => ID_imm,
        ID_PC_out => ID_PC
    );
    -- HAZARD detection logic, IDEX is flushed when PCSrc is asserted or CMUX is '0'

    ID_ctl_hazard <=
        X"00" when ((PCSrc = '1') or (cmux = '0')) else
        ID_ctl; --Gestisce Hazard e branch taken
    

    id_ex_reg: id_ex Port Map(
        clk => clk,
        IDEX_flush => PCSrc, -- Flush on branch taken
        IDEX_ctl_in => ID_ctl_hazard,
        IDEX_PC_in => ID_PC,
        IDEX_instr_in => ID_instr,
        IDEX_imm_in => ID_imm,
        IDEX_dataA_in => datars1,
        IDEX_dataB_in => datars2,

        IDEX_ctl_out => IDEX_ctl,
        IDEX_PC_out => IDEX_PC,
        IDEX_instr_out => IDEX_instr,
        IDEX_imm_out => IDEX_imm,
        IDEX_dataA_out => IDEX_datars1,
        IDEX_dataB_out => IDEX_datars2
    );

    ex_stage_map: ex_block Port Map(
        EX_PC_in => IDEX_PC,
        EX_instr_in => IDEX_instr,
        EX_imm_in => IDEX_imm,

        ID_dataA => IDEX_datars1,
        WB_dataA => WB_regreturn,
        MEM_dataA => EXMEM_alures,

        ID_dataB => IDEX_datars2,
        WB_dataB => WB_regreturn,
        MEM_dataB => EXMEM_alures,

        MEM_rd => EXMEM_rd, -- taken from stage registers
        WB_rd => WB_rd,
        MEM_RegWrite => EXMEM_ctl(1),
        WB_RegWrite => WB_ctl(1),

        ALUsrc => IDEX_ctl(5),
        ALUOp => IDEX_ctl(7 downto 6),

        ALUResult => ALUResult,
        BranchPC => BranchPC,
        WriteData => WriteData,
        BranchResult => BranchResult
        
    );

    ex_mem_reg: ex_mem Port Map(
        clk => clk,
        EXMEM_flush => PCSrc,

        EXMEM_ctrl_in => IDEX_ctl(4 downto 0),
        EXMEM_ALUresult_in => ALUResult,
        EXMEM_dataB_in => WriteData,
        EXMEM_rd_in => IDEX_instr(11 downto 7), -----
        EXMEM_branchPC_in => BranchPC,
        EXMEM_brtaken_in => BranchResult,

        EXMEM_ctrl_out => EXMEM_ctl,
        EXMEM_ALUresult_out => EXMEM_alures,
        EXMEM_dataB_out => EXMEM_wdata,
        EXMEM_rd_out => EXMEM_rd, -- Needed for Forwarding Logic
        EXMEM_branchPC_out => EXMEM_branchPC,
        EXMEM_brtaken_out => EXMEM_branchres
    );
    
    mem_stage_map: mem_block Port Map(
        MEM_clk => clk,
        MEM_branch_ctl => EXMEM_ctl(4),
        MEM_branch_true => EXMEM_branchres,
        MEM_memwrite => EXMEM_ctl(2),
        MEM_memread => EXMEM_ctl(3),
        MEM_address => EXMEM_alures,
        MEM_datawrite => EXMEM_wdata,

        MEM_readdata => MEM_readdata,
        MEM_aluresult => MEM_aluresult,
        PCSrc => PCSrc
    );

    mem_wb_reg: mem_wb Port Map(
        clk => clk,

        MEMWB_ctrl_in => EXMEM_ctl(1 downto 0),
        MEMWB_ALUresult_in => MEM_aluresult,
        MEMWB_readdata_in => MEM_readdata,
        MEMWB_rd_in => EXMEM_rd,

        MEMWB_ctrl_out => WB_ctl,
        MEMWB_ALUresult_out => WB_alu,
        MEMWB_readdata_out => WB_data,
        MEMWB_rd_out => WB_rd --Needed in forwarding
    );

    wb_mux: mux2to1_64 Port Map( --WB stage
        a => WB_alu,
        b => WB_data,
        s => WB_ctl(0),
        z => WB_regreturn
    );

    hazard_map: hazard Port Map(
        IDEX_memread => IDEX_ctl(3), -- On Load
        IDEX_rd => IDEX_instr(11 downto 7),
        IFID_rs1 => IFID_instr(19 downto 15),
        IFID_rs2 => IFID_instr(24 downto 20),
        cmux => cmux,
        PCwrite => PCwrite,
        IFID_write => IFID_write
    );


end architecture structural;
