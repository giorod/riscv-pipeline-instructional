library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

-- Branching logic, parallel to the ALU, so it uses the same forwarding logic ate the expense of more penalty cycle when branch is taken
-- Notice that this is the simpler logic possible, at least as I could think it

entity branching is
  port (
    funct3: in std_logic_vector(2 downto 0);
    dataA: in std_logic_vector(63 downto 0);
    dataB: in std_logic_vector(63 downto 0);
    branch: out std_logic := '0'
  );
end branching;

--  BRANCH  FUNCT3
--  beq     000
--  bne     001
--  blt     100
--  bge     101

architecture branch_behavior of branching is

    signal beq, bne, blt, bge: std_logic;

begin

    beq <= '1' when dataA = dataB else '0';
    bne <= '1' when dataA /= dataB else '0';
    blt <= '1' when signed(dataA) < signed(dataB) else '0';
    bge <= '1' when signed(dataA) >= signed(dataB) else '0';

    branch <= '1' when ((beq = '1' and funct3 = "000") or (bne = '1' and funct3 = "001") or (blt = '1' and funct3 = "100") or (bge = '1' and funct3 = "101")) else '0';

end branch_behavior ; -- branch_behavior