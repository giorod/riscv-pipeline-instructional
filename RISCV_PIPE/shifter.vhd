library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity shifter is
    port (
        data_in: in std_logic_vector(63 downto 0);
        data_out: out std_logic_vector(63 downto 0) := (others => '0')
    );
end entity shifter;

architecture behavioral of shifter is
    signal temp: std_logic_vector(63 downto 0);
begin
    
    temp <= data_in;
    data_out(0) <= '0';

    shifting: for i in 1 to 63 generate
        data_out(i) <= temp(i-1);
    end generate;
    
end architecture behavioral;