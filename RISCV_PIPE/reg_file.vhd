-------------------
-- REGISTER FILE --
-------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity reg_file is
    port (
    rs1: in std_logic_vector(4 downto 0); -- REGISTER ADDRESS rs1
    rs2: in std_logic_vector(4 downto 0); -- rs2
    rd: in std_logic_vector(4 downto 0); -- rd
    
    rs1_data: out std_logic_vector(63 downto 0) := (others => '0'); -- DATA 64BIT due output e un input di write
    rs2_data: out std_logic_vector(63 downto 0) := (others => '0');
    rd_data: in std_logic_vector(63 downto 0);

    regWrite: in std_logic
    );
end reg_file;

architecture behavioral of reg_file is
    
    type reg_type is array(0 to 31) of std_logic_vector(63 downto 0);
    signal register_file: reg_type := (others => (others => '0')); -- Initialize to 0 vectors

begin

    rs1_data <= register_file(to_integer(unsigned(rs1)));
    rs2_data <= register_file(to_integer(unsigned(rs2)));

    register_file(to_integer(unsigned(rd))) <= rd_data when regWrite = '1';

end architecture behavioral;
