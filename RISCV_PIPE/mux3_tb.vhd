library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity mux3_tb is
end;

architecture test of mux3_tb is
    component mux3to1_64 is
        port(
            a: in std_logic_vector(63 downto 0);
            b: in std_logic_vector(63 downto 0);
            c: in std_logic_vector(63 downto 0);
            s: in std_logic_vector(1 downto 0);
            z: out std_logic_vector(63 downto 0)
        );
    end component mux3to1_64;

    signal a,b,c,z: std_logic_vector(63 downto 0);
    signal s: std_logic_vector(1 downto 0);
    
begin
    
    stim: process
    begin
        wait for 100 ns;
        a <= x"0000000000000005";
        b <= x"0000000000000001";
        c <= x"0000000000000234";
        s <= "00";
        wait for 100 ns;
        a <= x"0000000000000005";
        b <= x"0000000000000001";
        c <= x"0000000000000234";
        s <= "01";
        wait for 100 ns;
        a <= x"0000000000000013";
        b <= x"0000000000000001";
        c <= x"0000000000000234";
        s <= "10";
        wait for 100 ns;
        a <= x"000000000000ABCD";
        b <= x"0000000000000001";
        c <= x"0000000000000234";
        s <= "00";
        
    end process stim;
    
    DUT: mux3to1_64
    Port Map(
        a => a,
        b => b,
        c => c,
        s => s,
        z => z
    );
    
end architecture test;
