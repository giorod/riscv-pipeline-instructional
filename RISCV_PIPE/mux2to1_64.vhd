library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity mux2to1_64 is
    port(
        a: in std_logic_vector(63 downto 0);
        b: in std_logic_vector(63 downto 0);
        s: in std_logic;
        z: out std_logic_vector(63 downto 0) := (others => '0')
    );
end entity mux2to1_64;


architecture behavioral of mux2to1_64 is

    component mux2to1 is
        port (
            x0: in std_logic;
            x1: in std_logic;
            s: in std_logic;
            y: out std_logic
        );
    end component mux2to1;

begin

    generation: for i in 0 to 63 generate
        singlebit: mux2to1 Port Map(x0 => a(i), x1 => b(i), s => s, y => z(i));
    end generate generation;
    
end architecture behavioral;