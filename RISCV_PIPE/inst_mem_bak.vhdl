------------------------
-- INSTRUCTION MEMORY --
------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity inst_mem is
	port (
        address: in std_logic_vector(63 downto 0) := (others => '0');
        outinst: out std_logic_vector(31 downto 0) := (others => '0')
    );
end;

architecture behavioral of inst_mem is 	

    type mem_type is array (0 to 127) of std_logic_vector(31 downto 0);

    signal mem_init: mem_type :=
    (   X"00000000",
    X"FF600793", X"00A00813",
    X"00000013", X"00000013",
    X"00000013", X"00000013",
    X"010788B3", X"00000000",
    X"00000000", X"00000000",
    X"00000000", X"00000000",
    X"00000000", X"00000000",
    X"00000000", X"00000000",
    X"00000000", X"00000000",
    X"00000000", X"00000000",
    X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000", 
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000", 
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000", 
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000", X"00000000",
     X"00000000"
     );

begin

    outinst <= mem_init(to_integer(unsigned(address(6 downto 2)))); -- L'indirizzo e' un intero unsigned a 64 bit, a blocchi di 4 byte -> nell'array ho step unitari

end behavioral;		