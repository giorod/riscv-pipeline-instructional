library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity mux3to1 is
	port (
        x0: in std_logic;
        x1: in std_logic;
        x2: in std_logic;
        s: in std_logic_vector(1 downto 0);
        y: out std_logic := '0'
    );
end mux3to1;

architecture behavioral of mux3to1 is
begin
    
    y <=    x0 when s="00" else
            x1 when s="01" else
            x2;
    
end architecture behavioral;