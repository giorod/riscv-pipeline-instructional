library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity mux2to1 is
	port (
        x0: in std_logic;
        x1: in std_logic;
        s: in std_logic;
        y: out std_logic := '0'
    );
end mux2to1;

architecture behavioral of mux2to1 is
begin
    
    y <=    x0 when s='0' else
            x1;
    
end architecture behavioral;