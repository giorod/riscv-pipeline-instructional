library IEEE;
use IEEE.std_logic_1164.all;

entity fwd_tb is
end entity;

architecture test of fwd_tb is

component forward is
  port (
    IDEX_rs1: in std_logic_vector(4 downto 0);
    IDEX_rs2: in std_logic_vector(4 downto 0);
    EXMEM_rd: in std_logic_vector(4 downto 0);
    MEMWB_rd: in std_logic_vector(4 downto 0);
    EXMEM_regwrite: in std_logic;
    MEMWB_regwrite: in std_logic;

    fwdA: out std_logic_vector(1 downto 0);
    fwdB: out std_logic_vector(1 downto 0)
  );
end component;

    signal rs1, rs2, EXrd, MEMrd: std_logic_vector(4 downto 0) := "00000";
    signal EXrw, MEMrw: std_logic := '0';
    signal A, B: std_logic_vector(1 downto 0);

begin

    stimulus: process
    begin

        wait for 100 ns; --exhazards

        rs1 <= "00001";
        rs2 <= "00000";
        EXrd <= "00001";
        MEMrd <= "00000";
        EXrw <= '1';
        MEMrw <= '0';

        wait for 100 ns;

        rs1 <= "00000";
        rs2 <= "00001";
        EXrd <= "00001";
        MEMrd <= "00000";
        EXrw <= '1';
        MEMrw <= '0';

        wait for 100 ns; --wbhazards

        rs1 <= "00001";
        rs2 <= "00000";
        EXrd <= "00011";
        MEMrd <= "00001";
        EXrw <= '0';
        MEMrw <= '1';

        wait for 100 ns;

        rs1 <= "00000";
        rs2 <= "00001";
        EXrd <= "00011";
        MEMrd <= "00001";
        EXrw <= '0';
        MEMrw <= '1';

        wait for 100 ns; --wb+exhazard cont

        rs1 <= "00001";
        rs2 <= "00011";
        EXrd <= "00001";
        MEMrd <= "00011";
        EXrw <= '1';
        MEMrw <= '1';

        wait for 100 ns; --wb+exhazard cont

        rs1 <= "00001";
        rs2 <= "00011";
        EXrd <= "00011";
        MEMrd <= "00011";
        EXrw <= '1';
        MEMrw <= '1';


        
    end process stimulus;

    DUT: forward
    Port Map(
        IDEX_rs1 => rs1,
        IDEX_rs2 => rs2,
        EXMEM_rd => EXrd,
        MEMWB_rd => MEMrd,
        EXMEM_regwrite => EXrw,
        MEMWB_regwrite => MEMrw,
    
        fwdA => A,
        fwdB => B
    );

end architecture;