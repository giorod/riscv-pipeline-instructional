library IEEE;
use IEEE.std_logic_1164.all;

-- FORWARDING UNIT --

entity forward is
  port (
    IDEX_rs1: in std_logic_vector(4 downto 0);
    IDEX_rs2: in std_logic_vector(4 downto 0);
    EXMEM_rd: in std_logic_vector(4 downto 0);
    MEMWB_rd: in std_logic_vector(4 downto 0);
    EXMEM_regwrite: in std_logic;
    MEMWB_regwrite: in std_logic;

    fwdA: out std_logic_vector(1 downto 0) := (others => '0');
    fwdB: out std_logic_vector(1 downto 0) := (others => '0')
  );
end forward;

architecture fwd_behavior of forward is

begin

    fwd_change: process(IDEX_rs1, IDEX_rs2, EXMEM_rd, MEMWB_rd, EXMEM_regwrite, MEMWB_regwrite) -- Is using a process the right choice?
    variable excA, excB: std_logic := '0'; -- NEEDED
    begin
        excA := '0';
        excB := '0';
        if ((EXMEM_regwrite = '1') and (EXMEM_rd /= "00000")) then
            if (EXMEM_rd = IDEX_rs1) then
                fwdA <= "10";
                excA := '1';
            end if;
            if (EXMEM_rd = IDEX_rs2) then
                fwdB <= "10";
                excB := '1';
            end if;
        end if;

        if ((MEMWB_regwrite = '1') and (MEMWB_rd /= "00000")) then
            if ((MEMWB_rd = IDEX_rs1) and not((EXMEM_rd = IDEX_rs1) and (EXMEM_regwrite = '1') and (EXMEM_rd /= "00000"))) then
                fwdA <= "01";
                excA := '1';
            end if;
            if ((MEMWB_rd = IDEX_rs2) and not((EXMEM_rd = IDEX_rs2) and (EXMEM_regwrite = '1') and (EXMEM_rd /= "00000"))) then
                fwdB <= "01";
                excB := '1';
            end if;
        end if;

        if (excA = '0') then
            fwdA <= "00";
        end if;
        if (excB = '0') then
            fwdB <= "00";
        end if;

    end process fwd_change;


end fwd_behavior ; -- fwd_behavior