library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity adder_64bit is
    port (
        a: in std_logic_vector(63 downto 0);
        b: in std_logic_vector(63 downto 0);
        s: out std_logic_vector(63 downto 0);
        issub: in std_logic;
        ovrf: out std_logic
    );
end entity adder_64bit;

architecture behavioral of adder_64bit is

component fulladder is port(
    a: in std_logic;
    b: in std_logic;
    cin: in std_logic;
    s: out std_logic;
    cout: out std_logic
);
end component fulladder;

signal carrysequence: std_logic_vector(63 downto 0);
    signal twocomp: std_logic_vector(63 downto 0);
begin
    
    twocomp <=  b when (issub = '0') else
                not(b);

    First_FA: fulladder Port Map(
        a => a(0),
        b => twocomp(0),
        cin => issub,
        s => s(0),
        cout => carrysequence(0)
    );

    AdderGenerator: for i in 63 downto 1 generate
        BitAdder: fulladder Port Map(
            a => a(i),
            b => twocomp(i),
            cin => carrysequence(i-1),
            s => s(i),
            cout => carrysequence(i)
        );
    end generate AdderGenerator;

    -- signed overflow condition

    ovrf <= carrysequence(63) xor carrysequence(62);
    
end architecture behavioral;