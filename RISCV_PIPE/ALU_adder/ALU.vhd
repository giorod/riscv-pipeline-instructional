library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity alu is
    port(
        alu_A: in std_logic_vector(63 downto 0);
        alu_B: in std_logic_vector(63 downto 0);
        alu_ctl: in std_logic_vector(3 downto 0);
        alu_res: out std_logic_vector(63 downto 0);
        alu_ovrf: out std_logic
    );
end alu;

architecture behavioral of alu is
    
    component adder_64bit is
        port (
            a: in std_logic_vector(63 downto 0);
            b: in std_logic_vector(63 downto 0);
            s: out std_logic_vector(63 downto 0);
            issub: in std_logic;
            ovrf: out std_logic
        );
    end component adder_64bit;

    signal alu_sum, alu_sub, result: std_logic_vector(63 downto 0);
    signal ovrf_sum, ovrf_sub, ovrf_res: std_logic;

begin

    sum_map: adder_64bit Port Map(
        a => alu_A,
        b => alu_B,
        s => alu_sum,
        issub => '0',
        ovrf => ovrf_sum
    );

    sub_map: adder_64bit Port Map(
        a => alu_A,
        b => alu_B,
        s => alu_sub,
        issub => '1',
        ovrf => ovrf_sub
    );

    with alu_ctl select 
    result <= alu_sum when "0010",
        alu_sub when "0110",
        (alu_A AND alu_B) when "0000",
        (alu_A OR alu_B) when "0001",
        x"0000000000000000" when others;
    alu_res <= result;

    with alu_ctl select 
    ovrf_res <= ovrf_sum when "0010",
        ovrf_sub when "0110",
        '0' when others;

    alu_res <= result;
    alu_ovrf <= ovrf_res;

end behavioral;