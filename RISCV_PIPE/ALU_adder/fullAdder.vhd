library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity fulladder is port(
    a: in std_logic;
    b: in std_logic;
    cin: in std_logic;
    s: out std_logic;
    cout: out std_logic
);
end entity fulladder;

architecture behavioral of fullAdder is
-- a   b   ci  S   co
-- 0   0   0   0   0   
-- 0   0   1   1   0   
-- 1   0   0   1   0   
-- 1   0   1   0   1   
-- 0   1   0   1   0   
-- 0   1   1   0   1   
-- 1   1   0   0   1   
-- 1   1   1   1   1
begin
    s <= a xor b xor cin;
    cout <= (a and b) or (a and cin) or (b and cin);
end architecture behavioral;   