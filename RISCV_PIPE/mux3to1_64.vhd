library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity mux3to1_64 is
    port(
        a: in std_logic_vector(63 downto 0);
        b: in std_logic_vector(63 downto 0);
        c: in std_logic_vector(63 downto 0);
        s: in std_logic_vector(1 downto 0);
        z: out std_logic_vector(63 downto 0) := (others => '0')
    );
end entity mux3to1_64;


architecture behavioral of mux3to1_64 is

    component mux3to1 is
        port (
            x0: in std_logic;
            x1: in std_logic;
            x2: in std_logic;
            s: in std_logic_vector(1 downto 0);
            y: out std_logic
        );
    end component mux3to1;

begin

    generation: for i in 0 to 63 generate
        singlebit: mux3to1 Port Map(x0 => a(i), x1 => b(i), x2 => c(i), s => s, y => z(i));
    end generate generation;
    
end architecture behavioral;