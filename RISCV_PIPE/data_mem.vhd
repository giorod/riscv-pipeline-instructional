-----------------
-- DATA MEMORY --
-----------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.Numeric_Std.all;

entity data_mem is
  port (
    clock: in std_logic;
    MemWrite: in std_logic;
    MemRead: in std_logic;
    address: in std_logic_vector(63 downto 0);
    writeData: in std_logic_vector(63 downto 0);
    readData: out std_logic_vector(63 downto 0) := (others => '0')
  );
end entity data_mem;

architecture RTL of data_mem is

   type ram_type is array (0 to 4096) of std_logic_vector(63 downto 0);
   signal ram : ram_type := (others => (others => '0'));
   signal read_address : std_logic_vector(address'range);

begin
  ram(to_integer(unsigned(address(14 downto 3)))) <= writeData when MemWrite='1';
  read_address <= address;
  readData <= ram(to_integer(unsigned(read_address(14 downto 3)))) when MemRead='1';

end architecture RTL;