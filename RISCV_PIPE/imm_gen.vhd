library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity imm_gen is
    port (
        imm_in: in std_logic_vector(31 downto 0);
        imm_out: out std_logic_vector(63 downto 0) := (others => '0')
    );
end entity imm_gen;

architecture behavioral of imm_gen is

    signal opcode: std_logic_vector(6 downto 0);
    signal immediate: std_logic_vector(11 downto 0);
    --signal result: std_logic_vector(51 downto 0);

begin

    opcode <= imm_in(6 downto 0);

    with opcode select -- costrutto select
        immediate <=    imm_in(31 downto 20) when "0010011", -- addi, andi, ori
                        imm_in(31 downto 20) when "0000011", -- ld
                        imm_in(31 downto 25) & imm_in(11 downto 7) when "0100011", -- sd
                        imm_in(31) & imm_in(7) & imm_in(30 downto 25) & imm_in(11 downto 8) when "1100011", -- branching
                        x"000" when others;

    --result <= (others => '0'); -- Sign extended immediate
    imm_out(11 downto 0) <= immediate;
    imm_out(63 downto 12) <= (63 downto 12 => immediate(11));

end behavioral ; -- behavioral