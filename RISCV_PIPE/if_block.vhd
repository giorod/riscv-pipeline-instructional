library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity if_block is
    port (
        clk: in std_logic;
        rst: in std_logic;
        PCwrite: in std_logic;
        PCsrc: in std_logic;
        BranchAddress: in std_logic_vector(63 downto 0);
        IF_instr: out std_logic_vector(31 downto 0) := (others => '0');
        IF_PC: out std_logic_vector(63 downto 0) := (others => '0')
    );
end entity if_block;

architecture IF_structure of if_block is
    
    component PC is
        port (
            clk: in std_logic;
            rst: in std_logic;
            PCwrite: in std_logic;
            address_in: in std_logic_vector(63 downto 0);
            address_out: out std_logic_vector(63 downto 0)
        );
    end component PC;

    component inst_mem is
        port (
            address: in std_logic_vector(63 downto 0);
            outinst: out std_logic_vector(31 downto 0)
        );
    end component;

    component mux2to1_64 is
        port(
            a: in std_logic_vector(63 downto 0);
            b: in std_logic_vector(63 downto 0);
            s: in std_logic;
            z: out std_logic_vector(63 downto 0)
        );
    end component mux2to1_64;

    component PC_adder is
        port (
            PC_prev: in std_logic_vector(63 downto 0);
            immediate: in std_logic_vector(63 downto 0);
            PC_next: out std_logic_vector(63 downto 0)
        );
    end component PC_adder;

    signal adder_out, mux_out, pc_out: std_logic_vector(63 downto 0);

begin
    
    PCmux: mux2to1_64 Port Map(
        a => adder_out,
        b => BranchAddress,
        s => PCsrc, -- asserted when branch is taken
        z => mux_out
    );

    PC_map: PC Port Map(
        clk => clk,
        rst => rst,
        PCwrite => PCwrite,
        address_in => mux_out,
        address_out => pc_out
    );

    adder_map: PC_adder Port Map(
        PC_prev => pc_out,
        immediate => x"0000000000000004",
        PC_next => adder_out
    );

    imem_map: inst_mem Port Map(
        address => pc_out,
        outinst => IF_instr
    );

    IF_PC <= pc_out;
    
end architecture IF_structure;