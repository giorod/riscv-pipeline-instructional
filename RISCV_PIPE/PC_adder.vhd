library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity PC_adder is
    port (
        PC_prev: in std_logic_vector(63 downto 0);
        immediate: in std_logic_vector(63 downto 0);
        PC_next: out std_logic_vector(63 downto 0) := (others => '0')
    );
end entity PC_adder;

architecture PC_increase of PC_adder is
    
begin
    
    PC_next <= std_logic_vector(signed(PC_prev) + signed(immediate));

end architecture PC_increase;  