library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity if_id is
	port (
        clk: in std_logic;
        IF_flush: in std_logic;
        IFID_write: in std_logic;
        instr_in: in std_logic_vector(31 downto 0);
        PC_in: in std_logic_vector(63 downto 0);
        instr_out: out std_logic_vector(31 downto 0) := (others => '0');
        PC_out: out std_logic_vector(63 downto 0) := (others => '0')
    );
end if_id;

architecture BEHAVIORAL of if_id is 	
begin

    if_id_update: process(clk)
    begin
        if rising_edge(clk) then
            if(IF_flush = '0' and IFID_write = '1') then
                instr_out <= instr_in;
                PC_out <= PC_in;
            elsif(IF_flush = '1') then
                instr_out <= (others => '0');
                PC_out <= (others => '0');
            end if;
        end if;
    end process if_id_update;

end BEHAVIORAL;