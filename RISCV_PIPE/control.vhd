------------------
-- CONTROL UNIT --
------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity control is
	port (
        opcode: in std_logic_vector(6 downto 0);
        ALUOp: out std_logic_vector(1 downto 0) := "00";
        ALUSrc: out std_logic := '0';
        Branch: out std_logic := '0';
        MemRead: out std_logic := '0';
        MemWrite: out std_logic := '0';
        RegWrite: out std_logic := '0';
        MemtoReg: out std_logic := '0'
    );
end control;

-- WANTED BEHAVIOR
-- Type     OPCODE
-- R-Type   0110011
-- ld       0000011
-- sd       0100011
-- SB-Type  1100011 JUMPS
-- addi, andi, 0010011 need ALUsrc asserted to 1 and you did as ALUOp = 11


architecture behavioral of control is
    signal fullcode: std_logic_vector(6 downto 0);
    signal sig_control: std_logic_vector(7 downto 0);
begin
    
    fullcode <= opcode;
    -- AluOp1 AluOp0 AluSrc Branch MemRead MemWrite RegWrite MemToReg
    with fullcode select 
        sig_control <=  "10000010" when "0110011",
                        "11100010" when "0010011", -- be careful to this line, I-type
                        "00101011" when "0000011",
                        "00100100" when "0100011",
                        "01010000" when "1100011",
                        "00000000" when others;
    
    ALUOp <= sig_control(7 downto 6);
    ALUSrc <= sig_control(5);
    Branch <= sig_control(4);
    MemRead <= sig_control(3);
    MemWrite <= sig_control(2);
    RegWrite <= sig_control(1);
    MemtoReg <= sig_control(0);

end architecture behavioral;