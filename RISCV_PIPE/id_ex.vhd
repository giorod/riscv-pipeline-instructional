library IEEE;
use IEEE.std_logic_1164.all;

entity id_ex is
	port (
        clk: in std_logic;
        IDEX_flush: in std_logic;

        IDEX_ctl_in: in std_logic_vector(7 downto 0);
        IDEX_PC_in: in std_logic_vector(63 downto 0);
        IDEX_instr_in: in std_logic_vector(31 downto 0);
        IDEX_imm_in: in std_logic_vector(63 downto 0);
        IDEX_dataA_in: in std_logic_vector(63 downto 0);
        IDEX_dataB_in: in std_logic_vector(63 downto 0);

        IDEX_ctl_out:   out std_logic_vector(7 downto 0) := (others => '0');
        IDEX_PC_out:    out std_logic_vector(63 downto 0) := (others => '0');
        IDEX_instr_out: out std_logic_vector(31 downto 0) := (others => '0');
        IDEX_imm_out:   out std_logic_vector(63 downto 0) := (others => '0');
        IDEX_dataA_out: out std_logic_vector(63 downto 0) := (others => '0');
        IDEX_dataB_out: out std_logic_vector(63 downto 0) := (others => '0')

    );
end entity;

architecture BEHAVIORAL of id_ex is 	
begin

    idexupdate: process(clk)
    begin
        if rising_edge(clk) then
            if IDEX_flush = '1' then
                IDEX_ctl_out <= "00000000";
                IDEX_PC_out <= IDEX_PC_in;
                IDEX_instr_out <= IDEX_instr_in;
                IDEX_imm_out <= IDEX_imm_in; 
                IDEX_dataA_out <= IDEX_dataA_in;
                IDEX_dataB_out <= IDEX_dataB_in;
            else
                IDEX_ctl_out <= IDEX_ctl_in;
                IDEX_PC_out <= IDEX_PC_in;
                IDEX_instr_out <= IDEX_instr_in;
                IDEX_imm_out <= IDEX_imm_in; 
                IDEX_dataA_out <= IDEX_dataA_in;
                IDEX_dataB_out <= IDEX_dataB_in;
            end if;
        end if;
    end process idexupdate;



end BEHAVIORAL;