library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity alu_control is
	port (
        funct7: in std_logic; -- Only one bit of funct7 field is asserted in R-type operation of SUB
        funct3: in std_logic_vector(2 downto 0);
        ALUOp: in std_logic_vector(1 downto 0);
        aluctl: out std_logic_vector(3 downto 0) := (others => '0')
    );
end alu_control;

-- Implementation of truth table @ page 253
--  OP  ALUOP   F3
--  ld  00      011
--  sd  00      011
--  R   10
--  I   11      UNUSED ALUOP FOR IMMEDIATE OPERATIONS
--  SB  01

architecture behavioral of alu_control is
begin
    aluctl <=   "0000" when (ALUOp = "10" or ALUOp = "11") and funct3 = "111" else    -- AND, ANDI
                "0001" when (ALUOp = "10" or ALUOp = "11") and funct3 = "110" else    -- OR, ORI
                "0010" when ALUOp = "00" else                                          -- LD, SD
                "0010" when ALUOp = "11" and funct3 = "000" else                       -- ADDI
                "0010" when ALUOp = "10" and funct7 = '0' and funct3 = "000" else      -- ADD
                "0110" when ALUOp = "10" and funct7 = '1' and funct3 = "000" else      -- SUB
                "0110" when ALUOp = "01" else                                          -- BEQ
                "0000";

end architecture behavioral;