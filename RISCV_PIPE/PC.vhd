library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity PC is
    port (
        clk: in std_logic;
        rst: in std_logic;
        PCwrite: in std_logic;
        address_in: in std_logic_vector(63 downto 0);
        address_out: out std_logic_vector(63 downto 0) := (others => '0')
    );
end entity PC;

architecture behavioral of PC is
    
begin
    
    PC_update: process(clk, rst)
    begin
        if rst = '1' then
            address_out <= (others => '0');
        elsif (rising_edge(clk) and (PCwrite = '1')) then
            address_out <= address_in;
        end if;
    end process PC_update;
    
end architecture behavioral;