library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity datapath_test is
end datapath_test;

architecture dp_test of datapath_test is

    component fulldatapath is
        port (
            clk: in std_logic;
            rst: in std_logic
        );
    end component fulldatapath;

    constant clk_period : time := 10 ns;
	signal clk, rst : std_logic;

begin

	test_map: fulldatapath port map(
	    clk => clk, 
    	rst => rst
	);

	clk_gen: process
	begin
	    clk <= '0';
	wait for clk_period/2;
	    clk <= '1';
	wait for clk_period/2;
	end process clk_gen;
	
	reset_gen: process 
	begin 
	rst <= '1';
	wait for clk_period;
	rst <= '0';
	wait for 300000 ns;
	end process reset_gen;


end dp_test ; -- dp_test
