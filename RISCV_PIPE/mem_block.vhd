library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity mem_block is
    port (
        MEM_clk: in std_logic;
        MEM_branch_ctl: in std_logic;
        MEM_branch_true: in std_logic;
        MEM_memwrite: in std_logic;
        MEM_memread: in std_logic;

        MEM_address: in std_logic_vector(63 downto 0);
        MEM_datawrite: in std_logic_vector(63 downto 0);

        MEM_readdata: out std_logic_vector(63 downto 0) := (others => '0');
        MEM_aluresult: out std_logic_vector(63 downto 0) := (others => '0');
        PCSrc: out std_logic := '0'

    );
end entity mem_block;

architecture structural of mem_block is

    component data_mem is
        port (
            clock: in std_logic;
            MemWrite: in std_logic;
            MemRead: in std_logic;
            address: in std_logic_vector(63 downto 0);
            writeData: in std_logic_vector(63 downto 0);
            readData: out std_logic_vector(63 downto 0)
        );
    end component data_mem;
    
begin
    
    data_mem_map: data_mem Port Map(
        clock => MEM_clk,
        MemWrite => MEM_memwrite,
        MemRead => MEM_memread,
        address => MEM_address,
        writeData => MEM_datawrite,
        readData => MEM_readdata
    );
    
    MEM_aluresult <= MEM_address;

    BranchAND: process(MEM_branch_true, MEM_branch_ctl)
    begin
        if ((MEM_branch_true = '1') and (MEM_branch_ctl = '1')) then
            PCSrc <= '1';
        else
            PCSrc <= '0';
        end if;
    end process;

end architecture structural;