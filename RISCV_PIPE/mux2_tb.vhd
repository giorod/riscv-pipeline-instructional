library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity mux_tb is
end;

architecture test of mux_tb is
    component mux2to1_64 is
        port(
            a: in std_logic_vector(63 downto 0);
            b: in std_logic_vector(63 downto 0);
            s: in std_logic;
            z: out std_logic_vector(63 downto 0)
        );
    end component mux2to1_64;

    signal a,b,z: std_logic_vector(63 downto 0);
    signal s: std_logic;
    
begin
    
    stim: process
    begin
        wait for 100 ns;
        a <= x"0000000000000005";
        b <= x"0000000000000001";
        s <= '0';
        wait for 100 ns;
        a <= x"0000000000000005";
        b <= x"0000000000000001";
        s <= '1';
        wait for 100 ns;
        a <= x"0000000000000013";
        b <= x"0000000000000001";
        s <= '0';
        wait for 100 ns;
        a <= x"000000000000ABCD";
        b <= x"0000000000000001";
        s <= '0';
        
    end process stim;
    
    DUT: mux2to1_64
    Port Map(
        a => a,
        b => b,
        s => s,
        z => z
    );
    
end architecture test;
