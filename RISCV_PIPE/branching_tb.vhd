library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity branch_tb is
end;

architecture test of branch_tb is

component branching is
    port (
      funct3: in std_logic_vector(2 downto 0) := (others => '0');
      dataA: in std_logic_vector(63 downto 0) := (others => '0');
      dataB: in std_logic_vector(63 downto 0) := (others => '0');
      branch: out std_logic := '0'
    );
  end component branching;


  signal A,B: std_logic_vector(63 downto 0);
  signal f3: std_logic_vector(2 downto 0);
  signal br: std_logic;

begin

    stim: process
    begin
        wait for 100 ns;--beq
        A <= X"00000000000000AA";
        B <= X"00000000000000AA";
        f3 <= "000";
        wait for 100 ns;--bne
        A <= X"00000000000000AA";
        B <= X"00000000000000AA";
        f3 <= "001";
        wait for 100 ns; --blt
        A <= X"00000000000000AA";
        B <= X"00000000000000AA";
        f3 <= "100";
        wait for 100 ns; --bge
        A <= X"0000000000000AAA";
        B <= X"00000000000000AA";
        f3 <= "101";
        wait for 100 ns;
        A <= X"00000000000000AA";
        B <= X"00000000000000A1";
        f3 <= "000";
        wait for 100 ns;--bne
        A <= X"00000000000000AC";
        B <= X"00000000000000AA";
        f3 <= "001";
        wait for 100 ns; --blt
        A <= X"0000000000000001";
        B <= X"00000000000000AA";
        f3 <= "100";
        wait for 100 ns; --bge
        A <= X"0000000000000001";
        B <= X"00000000000000AA";
        f3 <= "101";
    end process stim;


    DUT: branching
        Port Map(
          funct3 => f3,
          dataA => A,
          dataB => B,
          branch => br
        );
end test; -- test
