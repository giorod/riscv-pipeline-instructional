library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity ex_mem is
	port(
        clk: in std_logic;
        EXMEM_flush: in std_logic;
        
        EXMEM_ctrl_in:      in std_logic_vector(4 downto 0);
        EXMEM_ALUresult_in: in std_logic_vector(63 downto 0);
        EXMEM_dataB_in:     in std_logic_vector(63 downto 0);
        EXMEM_rd_in:        in std_logic_vector(4 downto 0);
        EXMEM_branchPC_in:  in std_logic_vector(63 downto 0);
        EXMEM_brtaken_in:   in std_logic;

        EXMEM_ctrl_out:      out std_logic_vector(4 downto 0) := (others => '0');
        EXMEM_ALUresult_out: out std_logic_vector(63 downto 0) := (others => '0');
        EXMEM_dataB_out:     out std_logic_vector(63 downto 0) := (others => '0');
        EXMEM_rd_out:        out std_logic_vector(4 downto 0) := (others => '0');
        EXMEM_branchPC_out:  out std_logic_vector(63 downto 0) := (others => '0');
        EXMEM_brtaken_out:   out std_logic := '0'

    );
end entity;

architecture BEHAVIORAL of ex_mem is 	
begin

    EXMEM_update: process(clk)
    begin
        if rising_edge(clk) then
            if EXMEM_flush = '1' then
                EXMEM_ctrl_out <= "00000";
                EXMEM_ALUresult_out <= EXMEM_ALUresult_in;
                EXMEM_dataB_out <= EXMEM_dataB_in;
                EXMEM_rd_out <= EXMEM_rd_in;
                EXMEM_branchPC_out <= EXMEM_branchPC_in;
                EXMEM_brtaken_out <= EXMEM_brtaken_in;
            else
                EXMEM_ctrl_out <= EXMEM_ctrl_in;
                EXMEM_ALUresult_out <= EXMEM_ALUresult_in;
                EXMEM_dataB_out <= EXMEM_dataB_in;
                EXMEM_rd_out <= EXMEM_rd_in;
                EXMEM_branchPC_out <= EXMEM_branchPC_in;
                EXMEM_brtaken_out <= EXMEM_brtaken_in;
                
            end if;
        end if;
    end process EXMEM_update;


end BEHAVIORAL;