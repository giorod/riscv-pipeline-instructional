library IEEE;
use IEEE.std_logic_1164.all;

entity hazard is
  port (
    IDEX_memread: in std_logic;
    IDEX_rd: in std_logic_vector(4 downto 0);
    IFID_rs1: in std_logic_vector(4 downto 0);
    IFID_rs2: in std_logic_vector(4 downto 0);
    cmux: out std_logic := '0';
    PCwrite: out std_logic := '0';
    IFID_write: out std_logic := '0'
  );
end entity hazard;

architecture behavioral of hazard is

  signal controls: std_logic;

begin
  
  controls <=
    '0' when ((IDEX_memread = '1') and ((IDEX_rd = IFID_rs1) or (IDEX_rd = IFID_rs2))) else
    '1';

  cmux <= controls;
  PCwrite <= controls;
  IFID_write <= controls;
  
end architecture behavioral;