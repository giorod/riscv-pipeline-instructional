library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity iftest is
end entity iftest;

architecture if_tb of iftest is
    
    component if_block is
        port (
            clk: in std_logic;
            rst: in std_logic;
            PCwrite: in std_logic;
            PCsrc: in std_logic;
            BranchAddress: in std_logic_vector(63 downto 0);
            IF_instr: out std_logic_vector(31 downto 0);
            IF_PC: out std_logic_vector(63 downto 0)
        );
    end component if_block;

    constant clk_period : time := 20 ns;
    signal clk, rst, PCwrite, PCsrc: std_logic;
    signal IF_PC, BranchAddress: std_logic_vector(63 downto 0);
    signal IF_instr: std_logic_vector(31 downto 0);

begin
    
    PCwrite <= '1';
    PCsrc <= '0';

	CLK_GEN: process
        begin
        clk <= '0';
        wait for clk_period/2;
        clk <= '1';
        wait for clk_period/2;
    end process CLK_GEN;
    
    stimulus_gen : process 
        begin 
        rst <= '1';
        wait for clk_period;
        rst <= '0';
        wait for 3000 ns;
    end process;


    if_map: if_block Port Map(
        clk => clk,
        rst => rst,
        PCwrite => PCwrite,
        PCsrc => PCsrc,
        BranchAddress => BranchAddress,
        IF_instr => IF_instr,
        IF_PC => IF_PC
    );
    
    
end architecture if_tb;