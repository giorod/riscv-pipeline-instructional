------------------------
-- INSTRUCTION MEMORY --
------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use std.textio.all;
use IEEE.std_logic_textio.all;

entity inst_mem is
	port (
        address: in std_logic_vector(63 downto 0);
        outinst: out std_logic_vector(31 downto 0) := (others => '0')
    );
end;

architecture behavioral of inst_mem is 	

    type mem_type is array (0 to 4095) of std_logic_vector(31 downto 0);

    impure function ReadMemFromFile (FileName : in string) return mem_type is
        FILE memfile : text is FileName;
        variable FileLine: line;
        variable mem : mem_type;

        begin

            for i in mem_type'range loop

                if(not endfile(memfile)) then
                    readline(memfile, FileLine);
                    hread(FileLine, mem(i));
                else
                    mem(i) := (others => '0');
                end if;

            end loop;

            return mem;
        end function;

    signal mem_init : mem_type := ReadMemFromFile("mem.data");

begin

    outinst <= mem_init(to_integer(unsigned(address(13 downto 2)))); -- L'indirizzo e' un intero unsigned a 64 bit, a blocchi di 4 byte -> nell'array ho step unitari

end behavioral;		