library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity alu_tb is
end entity;

architecture test of alu_tb is

    component alu is
        port(
        alu_A: in std_logic_vector(63 downto 0);
        alu_B: in std_logic_vector(63 downto 0);
        alu_ctl: in std_logic_vector(3 downto 0);
        alu_res: out std_logic_vector(63 downto 0);
        zero: out std_logic := '0'
    );
    end component;

signal a, b , res: std_logic_vector(63 downto 0);
signal ctl: std_logic_vector(3 downto 0);
signal zero: std_logic;

begin

    stimulus: process
    begin
        wait for 100 ns;
        a <= x"00000000000004D2";
        b <= x"00000000000002D3";
        ctl <= "0010";
        wait for 100 ns;
        a <= x"00000000000004D2";
        b <= x"00000000000002D3";
        ctl <= "0110";
        wait for 100 ns;
        b <= x"00000000000004D2";
        a <= x"00000000000002D3";
        ctl <= "0110";
        wait for 100 ns;
        b <= x"00000000000004D2";
        a <= x"00000000002D3000";
        ctl <= "0001";
        wait for 100 ns;
        a <= x"00000000000004D2";
        b <= x"00000000000004D2";
        ctl <= "0000";
        wait for 100 ns;

    end process stimulus;

    DUT: alu
    Port Map(
        alu_A => a,
        alu_B => b,
        alu_ctl => ctl,
        alu_res => res,
        zero => zero
    );

end architecture;