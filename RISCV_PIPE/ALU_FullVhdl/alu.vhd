library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity alu is
    port(
        alu_A: in std_logic_vector(63 downto 0);
        alu_B: in std_logic_vector(63 downto 0);
        alu_ctl: in std_logic_vector(3 downto 0);
        alu_res: out std_logic_vector(63 downto 0)
    );
end alu;

architecture behavioral of alu is
    signal aux: std_logic_vector(3 downto 0);
    signal result: std_logic_vector(63 downto 0);
begin
    with alu_ctl select 
    result <= std_logic_vector(signed(alu_A) + signed(alu_B)) when "0010",
        std_logic_vector(signed(alu_A) - signed(alu_B)) when "0110",
        (alu_A AND alu_B) when "0000",
        (alu_A OR alu_B) when "0001",
        x"0000000000000000" when others;
    alu_res <= result;
        
end behavioral;
