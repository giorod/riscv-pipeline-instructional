library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity ex_block is -- The stage registers are clock syncronized
    port (

        EX_PC_in: in std_logic_vector(63 downto 0);
        EX_instr_in: in std_logic_vector(31 downto 0); 
        EX_imm_in: in std_logic_vector(63 downto 0);

        ID_dataA: in std_logic_vector(63 downto 0);
        WB_dataA: in std_logic_vector(63 downto 0);
        MEM_dataA: in std_logic_vector(63 downto 0);

        ID_dataB: in std_logic_vector(63 downto 0);
        WB_dataB: in std_logic_vector(63 downto 0);
        MEM_dataB: in std_logic_vector(63 downto 0);

        MEM_rd: in std_logic_vector(4 downto 0); -- Forwarding inputs
        WB_rd: in std_logic_vector(4 downto 0);
        MEM_RegWrite: in std_logic;
        WB_RegWrite: in std_logic;

        ALUsrc: in std_logic;
        ALUOp: in std_logic_vector(1 downto 0);

        ALUResult: out std_logic_vector(63 downto 0) := (others => '0');
        BranchPC: out std_logic_vector(63 downto 0) := (others => '0');
        WriteData: out std_logic_vector(63 downto 0) := (others => '0');
        BranchResult: out std_logic := '0'

        -- ADD HAZARD OUTPUTS

    );
end entity ex_block;

architecture structural of ex_block is

    signal muxA_out, muxB_out, muximm_out, shifted_imm: std_logic_vector(63 downto 0);
    signal fwdA_out, fwdB_out: std_logic_vector(1 downto 0);
    signal alu_ctl_out: std_logic_vector(3 downto 0);
    
    component mux3to1_64 is
        port(
            a: in std_logic_vector(63 downto 0);
            b: in std_logic_vector(63 downto 0);
            c: in std_logic_vector(63 downto 0);
            s: in std_logic_vector(1 downto 0);
            z: out std_logic_vector(63 downto 0)
        );
    end component mux3to1_64;

    component mux2to1_64 is
        port(
            a: in std_logic_vector(63 downto 0);
            b: in std_logic_vector(63 downto 0);
            s: in std_logic;
            z: out std_logic_vector(63 downto 0)
        );
    end component mux2to1_64;
    
    component alu_control is
        port (
            funct7: in std_logic; -- Only one bit of funct7 field is asserted in R-type operation of SUB
            funct3: in std_logic_vector(2 downto 0);
            ALUOp: in std_logic_vector(1 downto 0);
            aluctl: out std_logic_vector(3 downto 0)
        );
    end component alu_control;

    component alu is
        port(
            alu_A: in std_logic_vector(63 downto 0);
            alu_B: in std_logic_vector(63 downto 0);
            alu_ctl: in std_logic_vector(3 downto 0);
            alu_res: out std_logic_vector(63 downto 0)
        );
    end component alu;

    component forward is
        port (
          IDEX_rs1: in std_logic_vector(4 downto 0);
          IDEX_rs2: in std_logic_vector(4 downto 0);
          EXMEM_rd: in std_logic_vector(4 downto 0);
          MEMWB_rd: in std_logic_vector(4 downto 0);
          EXMEM_regwrite: in std_logic;
          MEMWB_regwrite: in std_logic;
      
          fwdA: out std_logic_vector(1 downto 0);
          fwdB: out std_logic_vector(1 downto 0)
        );
    end component forward;

    component shifter is
        port (
            data_in: in std_logic_vector(63 downto 0);
            data_out: out std_logic_vector(63 downto 0)
        );
    end component shifter;

    component PC_adder is
        port (
            PC_prev: in std_logic_vector(63 downto 0);
            immediate: in std_logic_vector(63 downto 0);
            PC_next: out std_logic_vector(63 downto 0)
        );
    end component PC_adder;

    component branching is
        port (
          funct3: in std_logic_vector(2 downto 0) := (others => '0');
          dataA: in std_logic_vector(63 downto 0) := (others => '0');
          dataB: in std_logic_vector(63 downto 0) := (others => '0');
          branch: out std_logic := '0'
        );
    end component branching;
    --Add branch address calculator!!! done with pc adder
    
begin

    muxdataA: mux3to1_64 Port Map(
        a => ID_dataA,
        b => WB_dataA,
        c => MEM_dataA,
        s => fwdA_out,
        z => muxA_out
    );

    muxdataB: mux3to1_64 Port Map(
        a => ID_dataB,
        b => WB_dataB,
        c => MEM_dataB,
        s => fwdB_out,
        z => muxB_out
    );

    muxsrc: mux2to1_64 Port Map(
        a => muxB_out,
        b => EX_imm_in,
        s => ALUsrc,
        z => muximm_out
    );
    
    shifter_map: shifter Port Map(
        data_in => EX_imm_in,
        data_out => shifted_imm
    );

    branch_calculation: PC_adder Port Map(
        PC_prev => EX_PC_in,
        immediate => shifted_imm,
        PC_next => BranchPC
    );

    branching_component: branching Port Map(
        funct3 => EX_instr_in(14 downto 12),
        dataA => muxA_out,
        dataB => muxB_out,
        branch => BranchResult
    );

    ALU_map: alu Port Map(
        alu_A => muxA_out,
        alu_B => muximm_out,
        alu_ctl => alu_ctl_out,
        alu_res => ALUResult
    );

    ALUctl_map: alu_control Port Map(
        funct7 => EX_instr_in(30),
        funct3 => EX_instr_in(14 downto 12),
        ALUOp => ALUOp,
        aluctl => alu_ctl_out
    );

    forwarding_map: forward Port Map(
        IDEX_rs1 => EX_instr_in(19 downto 15),
        IDEX_rs2 => EX_instr_in(24 downto 20),
        EXMEM_rd => MEM_rd,
        MEMWB_rd => WB_rd,
        EXMEM_regwrite => MEM_RegWrite,
        MEMWB_regwrite => WB_RegWrite, 
        fwdA => fwdA_out,
        fwdB => fwdB_out
    );

    WriteData <= muxB_out;
    
end architecture structural;