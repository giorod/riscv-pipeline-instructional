library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity id_block is
    port (
        clk: in std_logic;
        ID_PC_in: in std_logic_vector(63 downto 0);
        ID_instr_in: in std_logic_vector(31 downto 0);
        regWrite: in std_logic; --comes from wb stage
        WriteData: in std_logic_vector(63 downto 0); -- comes from wb stage
        WriteAddr: in std_logic_vector(4 downto 0); -- comes from wb stage

        data1: out std_logic_vector(63 downto 0) := (others => '0');
        data2: out std_logic_vector(63 downto 0) := (others => '0');
        
        ID_instr_out: out std_logic_vector(31 downto 0) := (others => '0'); -- needed for the hazard detection and subsequent stages
        ID_ctl: out std_logic_vector(7 downto 0) := (others => '0'); -- control bits
        imm: out std_logic_vector(63 downto 0) := (others => '0'); -- calculated immediate, used in ex stage
        ID_PC_out: out std_logic_vector(63 downto 0) := (others => '0')
        --ADD HAZARD LOGIC

    );
end entity id_block;

architecture structural of id_block is

    component reg_file is
        port (
            rs1: in std_logic_vector(4 downto 0); -- REGISTER ADDRESS rs1
            rs2: in std_logic_vector(4 downto 0); -- rs2
            rd: in std_logic_vector(4 downto 0); -- rd
            
            rs1_data: out std_logic_vector(63 downto 0); -- DATA 64BIT due output e un input di write
            rs2_data: out std_logic_vector(63 downto 0);
            rd_data: in std_logic_vector(63 downto 0);
        
            regWrite: in std_logic
        );
    end component reg_file;

    component imm_gen is
        port (
            imm_in: in std_logic_vector(31 downto 0);
            imm_out: out std_logic_vector(63 downto 0)
        );
    end component imm_gen;

    component control is
        port (
            opcode: in std_logic_vector(6 downto 0);
            ALUOp: out std_logic_vector(1 downto 0);
            ALUSrc: out std_logic;
            Branch: out std_logic;
            MemRead: out std_logic;
            MemWrite: out std_logic;
            RegWrite: out std_logic;
            MemtoReg: out std_logic
        );
    end component control;
    
begin

    registerfile: reg_file Port Map(
        rs1 => ID_instr_in(19 downto 15),
        rs2 => ID_instr_in(24 downto 20),
        rd => WriteAddr,
        rs1_data => data1,
        rs2_data => data2,
        rd_data => WriteData,
        regWrite => regWrite
    );

    immediategeneration: imm_gen Port Map(
        imm_in => ID_instr_in,
        imm_out => imm
    );

    controlblock: control Port Map(
        opcode => ID_instr_in(6 downto 0),
        ALUOp => ID_ctl(7 downto 6),
        ALUSrc => ID_ctl(5),
        Branch => ID_ctl(4),
        MemRead => ID_ctl(3),
        MemWrite => ID_ctl(2),
        RegWrite => ID_ctl(1),
        MemtoReg => ID_ctl(0)
    );
    
    ID_PC_out <= ID_PC_in;
    ID_instr_out <= ID_instr_in;

end architecture structural;