.global _start

_start:

    # x0 e' il base address nel nostro caso
    nop

    addi t0, x0, 5
    sd t0, 0(x0)

    addi t0, x0, 2
    sd t0, 8(x0)

    addi t0, x0, 9
    sd t0, 16(x0)

    addi t0, x0, 1
    sd t0, 24(x0)

    addi t0, x0, -5
    sd t0, 32(x0)

	#addi t0, x0, 4
	#sd t0, 32(x0)

	#ld t0, 32(x0)

    # a0 = i, a1 = j, a2 = ilim, a3 = jlim

    addi a0, x0, -8
    addi a2, x0, 32 #ultimo indirizzo

outerloop:
    addi a0, a0, 8 # incremento i
    beq a0, a2, end
    addi a1, x0, -8 # inizializzo j = -1

innerloop:
    addi a1, a1, 8 # j++
    beq a1, a2, outerloop
    addi t0, a1, 8 # temp0 = j+1

    ld t1, 0(a1) # t1 = arr[j]
    ld t2, 0(t0) # t2 = arr[j+1]

    blt t2, t1, swap
    beq x0, x0, noswap

swap:
    
    sd t1, 0(t0)
    sd t2, 0(a1)

noswap:

    beq x0, x0, innerloop

end:

    nop
    nop
    nop
    nop
    ld t0, 0(x0)
    ld t0, 8(x0)
    ld t0, 16(x0)
    ld t0, 24(x0)
    ld t0, 32(x0)

