.global _start

_start:
    
    beq x0, x0, notrip

trip:
    nop
    nop
    nop
    nop
    nop
    #arithmetical ops
notrip:
    nop
    addi a0, x0, 123
    addi a1, x0, 23
    add a2, a0, a1
    sub a3, a1, a0
    andi a4, x0, 64
    ori a5, x0, 64
    and a6, a0, a1
    or a7, a0, a1

    #load and stores

    addi a0, x0, 256
    sd a0, 16(x0)
    ld a1, 16(x0)
    add a2, a1, a2


    addi a5, x0, -60
    addi a6, x0, 40

    blt a5, a6, trip
