.global _start

_start:
    nop
    addi a1, x0, 1
    addi a2, x0, 1

    sd a1, 0(x0)
    sd a2, 8(x0)

    addi a3, x0, 8
    addi a4, x0, 160

fibo:
    addi a3, a3, 8
    addi t0, a1, 0
    addi a1, a2, 0
    add a2, t0, a1

    sd a2, 0(a3)

    bne a3, a4, fibo

    addi a3, x0, -8

print:
    addi a3, a3, 8
    ld t0, 0(a3)
    bne a3, a4, print

end:
    nop
    nop
    nop
    nop
